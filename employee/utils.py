# coding=utf-8
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.views import exception_handler
from jsonfield import JSONField
import plotly.offline as opy
import plotly.graph_objs as go

__author__ = "Gahan"


class CustomAccessMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or request.user.is_staff:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class CustomJSONField(JSONField):
    def value_from_object(self, obj):
        """Return the value of this field in the given model instance."""
        return getattr(obj, self.attname)

    def value_to_string(self, obj):
        return dict(self.value_from_object(obj))


def plot_graph(x=None, y=None, marker=None, mode="lines+markers", name='Trace', **kwargs):
    marker = marker if marker else {'color': '#bc8e76', 'size': "10"}
    title = kwargs.get("title", "Analysis of Response")
    graph_type = kwargs.get("graph_type", "bar")
    _traces = kwargs.get("traces", None)
    _layout = kwargs.get("layout", None)
    x_title = kwargs.get("x_title", "X axis")
    y_title = kwargs.get("y_title", "Y axis")
    if not _traces or not _layout:
        if graph_type == "bar":
            _traces = _traces if _traces else [go.Bar(x=x, y=y, name=name)]
            _layout = go.Layout(title=title, barmode='group',
                                xaxis={'title': x_title, 'tickformat': ',d'},
                                yaxis={'title': y_title, 'tickformat': ',d'})
        else:  # if graph_type == "scatter"
            _traces = _traces if _traces else [go.Scatter(x=x, y=y, marker=marker, mode=mode, name=name)]
            _layout = go.Layout(title=title, xaxis={'title': x_title}, yaxis={'title': y_title})
    data = go.Data(_traces)
    layout = _layout
    figure = go.Figure(data=data, layout=layout)
    div = opy.plot(figure, auto_open=False, output_type='div')
    return div


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    try:
        errors = ["{} : {}".format(field.replace('_', ' '), value) for field, value in response.data.items()]
        error_string = ", ".join(errors)
    except Exception as e:
        error_string = "Unknown Exception: {}".format(e)

    if response is not None:
        # add the HTTP status code to the response.
        response.data['status'] = response.status_code
        if "non_field_errors" in response.data:
            # merge any non_field_errors in response
            response.data['detail'] = ", ".join(response.data['non_field_errors'])
        else:
            response.data['detail'] = error_string
    return response
