import os
import random
from faker import Faker


faker = Faker()


class DummyData(object):
    cities = ['malibu', 'tower', 'London', 'Gandhinagar', 'Ahmedabad', 'San Diego', 'New York']
    company_postfix = ['Co', 'Organization', 'Ltd.', 'Corporation', 'Management']
    password = "r@123456"
    path = '/home/quixom/Pictures/Wallpapers/'
    credentials = {
        "username": '+919999999901',
        "password": '1'
    }

    def generate_profile(self, gender="M", role=3):
        if gender == "F":
            _first_name, _last_name = faker.name_female().split()
        else:
            _first_name, _last_name = faker.name_male().split()
        return {
            "contact_number": self.get_phone_number(),
            "profile_image": self.get_profile_image(),
            "role": 3,
            "first_name": _first_name,
            "last_name": _last_name,
            "email": faker.email(),
            "password": 'r@123456',
            "gender": gender,
        }

    def generate_employee_profile(self):
        return {
            "job_title": faker.job(),
            "street": faker.street_address(),
            "zip_code": faker.zipcode(),
            # "city": faker.city(),
            "city": random.choice(self.cities),
            "country": faker.country(),
            # "category": faker.country(),
        }

    def get_profile_image(self):
        if os.path.exists(self.path):
            images = os.listdir(self.path)
            return os.path.join(self.path, random.choice(images))
        else:
            return None

    @staticmethod
    def get_phone_number():
        return "+91{}".format(random.randint(9999990000, 9999999999))
