import os

flag = True  # flag to set server settings
# flag=False  # flag to set server settings
# if os.sys.platform == 'linux':
#     host_name = os.popen('hostname -I').read().strip()
#     if host_name.startswith('192.'):
#         from .local_settings import *
#     else:
#         from .server_settings import *
# else:
#     from .local_settings import *

if flag:
    from .server_settings import *
    # print("working with server_settings")
else:
    from .local_settings import *
    # print("working with local_settings")
