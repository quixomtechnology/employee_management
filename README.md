﻿Anonymous Activity
================
- Register User select plan  
  : URI : /select-plan/
  : `main.views.PlanSelector`
- Login if already registered
  : URI : /login/

> Permission Details: Only HR is allowed to access web portal

Home
=====
>`employee.views.HomePageView`

Employee Data
-------------------
> `employee.views.EmployeeDataView`
### Upload Data
- Bulk upload user data
  : `employee.views.FileUploadView`
  - HR will upload csv 
### View Data
- View user data
  : `employee.views.EmployeeDataList`
- Edit employee view
  : `employee.views.EditEmployeeView`
- Edit user profile
  : `employee.views.EditUserView`
  : `employee.views.PasswordResetView`
- Delete Employee
  : `employee.views.EmployeeDeleteView`

Field Rate
------------
### Chat
: `chat.views.ChatView`
### News Feed
- Create NewsFeed
  : `employee.views.CreateNewsFeed`
- Manage News Feed
  : `employee.views.NewsFeedManager`
  : `employee.views.NewsFeedDeleteView`
  
### Survey
- Add Question
  : `employee.views.AddQuestion`
- Add Survey
  : `employee.views.CreateSurvey`
- Manage Survey
  : `employee.views.SurveyManager`
  : `employee.views.SurveyDeleteView`
  > lists survey with progress status 

### Benchmark

- Benchmark of survey
  : `employee.views.SurveyBenchmark`
  : accordin for survey and list of question
- Google Map BenchmarkMap
  : `employee.views.BenchmarkMap`
- Question Graph
  : `employee.views.QuestionGraph`

Settings
----------
  : `employee.views.SettingsView`
### Profile Update
- Edit own user profile
  : `employee.views.EditUserView`
  : `employee.views.PasswordResetView`
### Create User
  :`employee.views.CreateUserView`
### Activity Log
  :`employee.views.ActivityMonitorView`

API Guide
========
Login API  
------------
### Validate user
|  |  |
|--|--|
| url | `/api-token-auth/` |
| method | `POST` |
| Content-Type | `application/json` |

##### POST DATA:  
```json  
{  
 "username":"+919999999902",  // contact number of user
 "password":"c4ca4238a0b923820dcc509a6f75849b"  // password (md5 hash of plaintext)
}
```  
  
##### Response:  
> Success
```json  
{  
 "contact_number": "+919999999902",  // Primary Contact Number/Phone number of user 
 "id": 32, // ID of user/employee
 "first_name": "Stephen",  // First Name of user
 "last_name": "Strange",  // Last Name of user
 "profile_image":"http://103.77.126.251:2218/files/media/uploads/temp_LAProiE.jpeg",  // User's profile image
 "role": 3,  // Role of user - By default all employee role will be 3 
 "hr_name": "Bruce",  // Name of HR
 "hr_id": 31,  // Id of HR  
 "hr_profile_image": "http://103.77.126.251:2218/files/media/uploads/iron_man_by_alexiscabo1-da2oe2u.png", // HR profile image : empty string if profile_image not available 
 "token": "ab549237d4e38ba4241544d3ae8e312e3f2101b1"  // Token for user  
}
```  
> Failure:  
```json  
{  
 "non_field_errors": [ "Unable to log in with provided credentials." ], 
 "status": 400, 
 "detail": "Unable to log in with provided credentials."
}  
```  
  
Survey API Doc  
-------------------
### Get list of available surveys  
|  |  |
|--|--|
| url | `/api/v1/survey/` |
| method | `GET` |
| Content-Type | `application/json` |
| Authorization | `Token ab549237d4e38ba4241544d3ae8e312e3f2101b1` |

##### Response: json  
```json  
{  
 "count": 4,  // total number of surveys 
 "next": "http://192.168.5.9:8889/api/v1/survey/?limit=2&offset=2",  // url to next page 
 "previous": null,  // url to previous page 
 "results": [  // contains entries of all survey in current page result 
 { 
 "url": "http://192.168.5.9:8889/api/survey/1/",  // url of survey 
 "id": 1,  // id of survey 
 "name": "General",  // survey name "employee_group": "gotham",  // employee group 
 "question": [  // lists all the question objects linked to the survey ], 
 "steps": 5,  // N/A steps completed in survey (shows progress of survey)
 "complete": true,  // N/A survey is completed (contains all 5 stages)
 "start_date": "2017-12-28T23:59:00Z",  //  N/A not to start survey before this time 
 "end_date": "2018-01-01T23:59:00Z",  //  N/A deadline to complete survey 
 "created_by": {},  // N/A user/HR detail who created this survey
 "start_time": "1514764800",  // start epoch time "end_time": "1517443140",  // end_epoch time 
 "current_time": 1515571139.2649748,  // current epoch time
 "total_question": 2, // total question in the survey
 "responded": false,  // value of survey is responded by requested user or not
 "benchmark": {  // benchmark detail of survey (blank dict if not HR)
	 "1":  {  // question id
		 "total_responses":  1,  // total responses
		 "average_rating":  21.0,  // average rating 
		 "rates":  {  "1":  2,  "5":  5  },  
		 "cities":  ["Gandhinagar", "London", "malibu", "tower"],
		 "city_response":  [
			 {"responses": 1, "city": "Gandhinagar"},
			 {"responses": 1, "city": "London"},
			 {"responses":  2, "city": "malibu"}
		 ],
		 "question_title": "How was your day today?",
		 "rating": 21,
		 "rate_scale": 5
	 },
	 "count":  1
 }]
 }  
```  
-----  
### Get detail of survey  
|  |  |
|--|--|
| url | `/api/v1/survey/1/` |
| method | `GET` |
| Content-Type | `application/json` |
| Authorization | `Token ab549237d4e38ba4241544d3ae8e312e3f2101b1` |
```json  
{  
 "url": "http://192.168.5.9:8889/api/survey/1/",  // url of survey 
 "id": 1,  // id of survey 
 "name": "General",  // survey name 
 "employee_group": "gotham",  // employee group 
 "question": [  // lists all the question objects linked to the survey 
	 { 
		 "url": "http://192.168.5.9:8889/api/question_database/1/",  // N/A 
		 "question_id": 1,  // question_id 
		 "question_title": "How was your day today?",  // question_title 
		 "answer_type": 1,  // N/A for now.. 1 means rating type 
		 "options": 5,  // maximum rate_scale_value 
		 "asked_by": [ // N/A 6 ] 
	 }, 
	 { 
		 "url": "http://192.168.5.9:8889/api/question_database/2/",  // N/A 
		 "question_id": 2, 
		 "question_title": "How was your experience with clients?", 
		 "answer_type": 1, 
		 "options": 7,  // maximum rate_scale_value 
		 "asked_by": [  // N/A 6 ] 
	 } 
 ], 
 "steps": 5,  // N/A steps completed in survey 
 "complete": true,  // N/A 
 "start_date": "2017-12-28T23:59:00Z",  //  N/A not to start survey before this time 
 "end_date": "2018-01-01T23:59:00Z",  //  N/A deadline to complete survey 
 "created_by": {},  //  N/A 
 "start_time": "1514764800",  // start epoch time 
 "end_time": "1517443140",  // end_epoch time 
 "current_time": 1515571139.2649748,  // current epoch time 
 "total_question": 2, // total question in the survey
 "responded": false,  // value of survey is responded by requested user or not
}  
```  
-----  
### Post response of survey
|  |  |
|--|--|
| url | `/api/v1/survey/response/` |
| method | `POST` |
| Content-Type | `application/json` |
| Authorization | `Token ab549237d4e38ba4241544d3ae8e312e3f2101b1` |  
##### Post Data:
```json  
{  
 "survey_id": 1,  // id of survey 
 "answers": 
	 {
		"1": {  // key is question id 
			 "r": 2  // rated value by user 
		}, 
		"2": {  // key is question id 
			"m": "user entered message",  // optional message 
			"r": 2  // rated value by user 
		} 
	}, 
"complete": true  // show status if survey is completed
}
```  

Directory Structure
===============
    .  
    ├── chat                            # chat app  mapped at /chat/  
    │   ├── __init__.py  
    │   ├── templates  
    │   │   └── chat.html  
    │   ├── urls.py  
    │   └── views.py  
    ├── db.sqlite3                      # local temp db  
    ├── employee                        # all the activity to be performed by HR/specific company will be created here  
    │   ├── admin.py  
    │   ├── apps.py  
    │   ├── fixtures                    # generated fixtures to run tests  
    │   │   ├── data_dump.json  
    │   │   └── usermodel.json  
    │   ├── __init__.py  
    │   ├── migrations                  # migrations  
    │   │   ├── 0001_initial.py  
    │   │   ├── 0002_auto_20180426_0700.py  
    │   │   └── __init__.py  
    │   ├── models.py  
    │   ├── serializers.py              # serializers  
    │   ├── tables.py  
    │   ├── templates  
    │   │   ├── employee_data  
    │   │   │   ├── edit_user.html          # edit user crispy form  
    │   │   │   ├── employee_data.html      # EMPLOYEE DETAIL section  
    │   │   │   ├── employee_detail.html    # display employee data (list of employees with search functionality)  
    │   │   │   └── file_upload.html        # file upload to create bulk user  
    │   │   ├── field_rate  
    │   │   │   ├── benchmark  
    │   │   │   │   ├── benchmark.html      # renders survey and question dynamically from API call with jquery  
    │   │   │   │   ├── graph.html          # renders graph from plotly  
    │   │   │   │   └── map.html            # displays map of responses by city using google API  
    │   │   │   ├── create_news_feed.html   # create news feed crispy form  
    │   │   │   ├── field_rate.html         # FIELDRATE section  
    │   │   │   ├── news_feed.html          # display list of created news feed / allow to broadcast  
    │   │   │   └── survey  
    │   │   │       ├── add_question.html   # pop up template used to add html question  
    │   │   │       ├── add_survey.html     # add survey html  
    │   │   │       └── survey.html         # display list of survey created with progress  
    │   │   ├── home.html                   # HOME page  
    │   │   └── settings  
    │   │       ├── activity_log.html       # display tables of activity log performed by user  
    │   │       ├── create_user.html        # create user crispy form  
    │   │       └── settings.html           # SETTINGS section  
    │   ├── tests  
    │   │   ├── __init__.py  
    │   │   ├── populate_dummy_data.py  # ignore/unfinished  
    │   │   └── utils.py  
    │   ├── tests.py  
    │   ├── urls.py  
    │   ├── utils.py                    # utility like rendering plotly graph are written here  
    │   ├── viewsets.py                 # API view nad viewsets  
    │   └── views.py  
    ├── employee_management  
    │   ├── __init__.py  
    │   ├── local_settings.py  
    │   ├── plotly_config.py            # plotly config  # TODO: change with client credential  
    │   ├── server_settings.py  
    │   ├── serviceAccountKey.json  
    │   ├── settings.py  # project settings  
    │   ├── urls.py  
    │   └── wsgi.py  
    ├── forms                           # all the forms(mostly crispy forms) are stored here  
    │   ├── common.py  
    │   ├── company.py  
    │   ├── helpers.py  
    │   └── __init__.py  
    ├── main  # a main app having core functionality performed by fieldrate ADMIN / developer  
    │   ├── admin.py  
    │   ├── apps.py  
    │   ├── context_processors.py       # custom context processor to be render in template like site name  
    │   ├── firebase_config.py  
    │   ├── fixtures                    # generated fixtures to run tests  
    │   │   ├── auth.json  
    │   │   ├── data_dump.json  
    │   │   ├── dump.json  
    │   │   └── main.json  
    │   ├── __init__.py  
    │   ├── migrations                  # migrations  
    │   │   ├── 0001_initial.py  
    │   │   └── __init__.py  
    │   ├── models.py  
    │   ├── serializers.py  
    │   ├── templatetags  
    │   │   ├── custom_tags.py          # custom template tags  
    │   │   └── __init__.py  
    │   ├── tests.py  
    │   ├── urls.py  
    │   ├── utility.py  
    │   └── views.py  
    ├── manage.py  
    ├── mysql.conf  
    ├── README.md  
    ├── requirements.txt  
    ├── server_dump                     # stores csv for failed bulk user creation  
    │   ├── failure_2017_12_06_12:30:47.csv  
    │   └── failure_2018_03_27_10:35:30.csv  
    ├── templates  
    │   ├── admin  
    │   │   └── base.html               # override base admin  
    │   ├── common  
    │   │   ├── common.html             # common html extended in all other html file  
    │   │   ├── common_nav.html         # common nav bar included in common.html  
    │   │   ├── done.html  
    │   │   ├── error_doc.html  
    │   │   ├── footer.html             # common footer included in common.html  
    │   │   ├── login.html              # login page  
    │   │   └── message.html            # included to render messages/error template  
    │   ├── company  
    │   │   └── _add_survey.html  
    │   ├── django_tables2  
    │   │   └── table.html              # override django tables2 template  
    │   ├── field_rate  
    │   │   ├── create_company.html     # create company /HR user form  
    │   │   └── purchase_plan.html      # select-plan # auto create HR user  
    │   └── rest_framework  
    │       └── base.html  
    └── userfiles  
        ├── media  
        │   └── uploads                 # user uploaded files stored here # ignored from git  
        │       ├── 156M0g_3DVjjbZ.jpg  
        │       ├── 156M0g.jpg  
        │       ├── 156M0g_JWrZARR.jpg  
        │       ├── 501E0F9394C6D5E26A57FD99FD1251B3DD870E2DD6533DA629pimgpsh_fullsize_distr.png  
        │       ├── 5c53a2d79182a71a8adb0408ae21f710-d359j0j.jpg  
        │       ├── 89b7c1e138635d3d130f680dba18e115.jpg  
        │       ├── age_of_ultron__iron_man_by_smlshin-d8k5mry.jpg  
        │       ├── age_of_ultron__iron_man_by_smlshin-d8k5mry_My52JPp.jpg  
        │       ├── age_of_ultron__iron_man_by_smlshin-d8k5mry_YXiBFbC.jpg  
        │       ├── arrow_png_by_buffy2ville-da57jg4_LPz69Vx.png  
        │       ├── arrow_png_by_buffy2ville-da57jg4.png  
        │       ├── art_deco_batman_by_rodolforever-d21chyp.jpg  
        │       ├── batman_by_mo_stylez09_gvqCQIt.jpg  
        │       ├── batman_by_mo_stylez09.jpg  
        │       ├── google_places_2fSi5Pw.png  
        │       ├── google_places_9Jcq18L.png  
        │       ├── google_places.png  
        │       ├── heros.gif  
        │       ├── images_3.jpg  
        │       ├── ironman_civil_war_by_spidertof-da15vbu.jpg  
        │       ├── iron_man_mk_24__transparent_background__by_camo_flauge-d9n1n38.png  
        │       ├── square.jpg  
        │       ├── temp_0VIy7DH.jpeg  
        │       ├── temp_1uDJNJb.jpeg  
        │       ├── temp_2096AUN.jpeg  
        │       ├── temp_2zSIL5B.jpeg  
        │       ├── temp_3sqi142.jpeg  
        │       ├── temp_4fJqD5M.jpeg  
        │       ├── temp_5QMrRPF.jpeg  
        │       ├── temp_67AIx72.jpeg  
        │       ├── temp_JmVoF5Y.jpeg  
        │       ├── temp_JOVqXYZ.jpeg  
        │       ├── temp.jpeg  
        │       ├── temp_k2uA07r.jpeg  
        │       ├── temp_kKMsmo2.jpeg  
        │       ├── temp_KSYBVhK.jpeg  
        │       └── x_men_s_wolverine_2__transparent_background__by_camo_flauge-d9lclf9.png  
        └── selenium  


